# rpg : traitement de données du Registre Parcellaire Graphique

Scripts en environnement Windows 10 : MinGW R MikTex

## Scripts R
Ces scripts sont dans le dossier "scripts".

## Tex
Le langage Tex est utilisé pour la production des documents.

MikTex est l'environnement utilisé.

Les fichiers .tex sont dans le dossier RPG.
